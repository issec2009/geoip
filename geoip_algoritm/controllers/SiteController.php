<?php

namespace app\controllers;

use Yii;
use yii\redis\Cache;
use yii\web\Controller;
use yii\web\HttpException;
use MaxMind\Db\Reader;

class SiteController extends Controller
{

    /**
     * Displays ip info.
     *
     * @return string
     * @throws HttpException
     * @throws Reader\InvalidDatabaseException
     * @throws \Exception
     */
    public function actionIndex($ip = '')
    {

        $ipAddress = $ip;

        $cache = new Cache();
        $final_ip_info = $cache->get($ipAddress);

        if ($final_ip_info) {

            $respons = ['from Redis cache', 'ip_info' => $final_ip_info];

        } else {

            $reader = new Reader(Yii::getAlias('@app').'/components/GeoIP2/GeoLite2-City.mmdb');
            $ip_info = $reader->get($ipAddress);
            $reader->close();


            if (!isset($ip_info['location']['latitude']) ||
                !isset($ip_info['location']['longitude']) ||
                !isset($ip_info['country']['names']['en']) ||
                !isset($ip_info['city']['names']['en']))
                throw new HttpException(404, 'Ip not found');

            $final_ip_info = [
                'latitude' => $ip_info['location']['latitude'],
                'longitude' => $ip_info['location']['longitude'],
                'country' => $ip_info['country']['names']['en'],
                'city' => $ip_info['city']['names']['en']
            ];

            // Кешируем на 30 мин.
            $cache->set($ipAddress, $final_ip_info, 1800);

            $respons = ['from MaxMind DB', 'ip_info' => $final_ip_info];

        }

        \Yii::$app->response->data = $respons;

    }

}
