# README #

Необходимо создать микросервис GeoIP.   
https://docs.nexcess.net/article/what-is-geoip.html     
Микросервис должен принимать запрос на получение информации GET /ip2geo?ip=x.x.x.x и в ответ возвращать JSON с широтой, долготой и названиями страны и города на английском языке.  
Если IP адрес не найден, то должен возвращаться пустой ответ с кодом 404.   
Для ускорения работы сервис должен кэшировать результаты запросов на 30 минут. База данных адресов должна находиться внутри микросервиса.   

### How To Use ###

* Create image   
docker build -t geoip .  

* Start container "service"     
docker run -itd -p 80:80 geoip      
or interactivly     
docker run -it -p 80:80 geoip bash

* Another way  
docker run -it -p 80:80 issec2009/geoip bash 

* Repo       
https://hub.docker.com/r/issec2009/geoip/
